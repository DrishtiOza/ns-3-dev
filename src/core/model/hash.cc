/*
 * Copyright (c) 2012 Lawrence Livermore National Laboratory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Peter D. Barnes, Jr. <pdbarnes@llnl.gov>
 */

#include "hash.h"

#include "log.h"

/**
 * \file
 * \ingroup hash
 * \brief ns3::Hasher implementation.
 */

namespace ns3
{

NS_LOG_COMPONENT_DEFINE("Hash");

Hasher&
GetStaticHash()
{
    static Hasher g_hasher = Hasher();
    g_hasher.clear();
    return g_hasher;
}

<<<<<<< HEAD
Hasher& GetStaticHash (void)
{
  static Hasher g_hasher = Hasher ();
  g_hasher.clear();
  return g_hasher;
}

Hasher::Hasher ()
=======
Hasher::Hasher()
>>>>>>> f47dd1e70633cb9db01a679d5c0be1a3e1bcce06
{
    m_impl = Create<Hash::Function::Murmur3>();
    NS_ASSERT(m_impl);
}

Hasher::Hasher(Ptr<Hash::Implementation> hp)
    : m_impl(hp)
{
    NS_ASSERT(m_impl);
}

Hasher&
Hasher::clear()
{
    m_impl->clear();
    return *this;
}

} // namespace ns3
