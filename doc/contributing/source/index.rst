.. only:: html or latex

Contributing to ns-3
====================

<<<<<<< HEAD
This is the guide to *Contributing to ns-3*. Primary documentation for the ns-3 project is available in several forms:

* `ns-3 Doxygen <http://www.nsnam.org/doxygen/index.html>`_: Documentation of the public APIs of the simulator
* Tutorial for the `latest release <http://www.nsnam.org/documentation/latest/>`_ and `development tree <http://www.nsnam.org/ns-3-dev/documentation/>`_
* Contributing to ns-3 (this document)
* Manual, and Model Library for the `latest release <http://www.nsnam.org/documentation/latest/>`_ and `development tree <http://www.nsnam.org/ns-3-dev/documentation/>`_
* `ns-3 wiki <http://www.nsnam.org/wiki/Main_Page>`_

This document is written in `reStructuredText <http://docutils.sourceforge.net/rst.html>`_ for `Sphinx <http://sphinx.pocoo.org/>`_ and is maintained in the
``doc/contributing`` directory of ns-3's source code.
=======
This is the *ns-3 Contributing Guide*. Primary documentation for the ns-3 project is organized as
follows:

* Several guides that are version controlled for each release (the
  `latest release <https://www.nsnam.org/documentation/latest/>`_) and
  `development tree <https://www.nsnam.org/ns-3-dev/documentation/>`_:

  * Tutorial
  * Installation Guide
  * Manual
  * Model Library
  * Contributing Guide *(this document)*
* `ns-3 Doxygen <https://www.nsnam.org/docs/doxygen/index.html>`_: Documentation of the public APIs of
  the simulator
* `ns-3 wiki <https://www.nsnam.org/wiki/Main_Page>`_

This document is written in `reStructuredText <http://docutils.sourceforge.net/rst.html>`_ for `Sphinx <https://www.sphinx-doc.org/>`_ and is maintained in the
``doc/contributing`` directory of ns-3's source code.  Source file column width is 100 columns.
>>>>>>> f47dd1e70633cb9db01a679d5c0be1a3e1bcce06

.. toctree::
   :maxdepth: 2

   introduction
   general
   enhancements
   models
   external
   coding-style
