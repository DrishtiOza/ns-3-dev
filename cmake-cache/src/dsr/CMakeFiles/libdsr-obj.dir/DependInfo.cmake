# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/drishti/workspace/ns-allinone-3.36.1/ns-3.36.1/src/dsr/helper/dsr-helper.cc" "/home/drishti/workspace/ns-allinone-3.36.1/ns-3.36.1/cmake-cache/src/dsr/CMakeFiles/libdsr-obj.dir/helper/dsr-helper.cc.o"
  "/home/drishti/workspace/ns-allinone-3.36.1/ns-3.36.1/src/dsr/helper/dsr-main-helper.cc" "/home/drishti/workspace/ns-allinone-3.36.1/ns-3.36.1/cmake-cache/src/dsr/CMakeFiles/libdsr-obj.dir/helper/dsr-main-helper.cc.o"
  "/home/drishti/workspace/ns-allinone-3.36.1/ns-3.36.1/src/dsr/model/dsr-errorbuff.cc" "/home/drishti/workspace/ns-allinone-3.36.1/ns-3.36.1/cmake-cache/src/dsr/CMakeFiles/libdsr-obj.dir/model/dsr-errorbuff.cc.o"
  "/home/drishti/workspace/ns-allinone-3.36.1/ns-3.36.1/src/dsr/model/dsr-fs-header.cc" "/home/drishti/workspace/ns-allinone-3.36.1/ns-3.36.1/cmake-cache/src/dsr/CMakeFiles/libdsr-obj.dir/model/dsr-fs-header.cc.o"
  "/home/drishti/workspace/ns-allinone-3.36.1/ns-3.36.1/src/dsr/model/dsr-gratuitous-reply-table.cc" "/home/drishti/workspace/ns-allinone-3.36.1/ns-3.36.1/cmake-cache/src/dsr/CMakeFiles/libdsr-obj.dir/model/dsr-gratuitous-reply-table.cc.o"
  "/home/drishti/workspace/ns-allinone-3.36.1/ns-3.36.1/src/dsr/model/dsr-maintain-buff.cc" "/home/drishti/workspace/ns-allinone-3.36.1/ns-3.36.1/cmake-cache/src/dsr/CMakeFiles/libdsr-obj.dir/model/dsr-maintain-buff.cc.o"
  "/home/drishti/workspace/ns-allinone-3.36.1/ns-3.36.1/src/dsr/model/dsr-network-queue.cc" "/home/drishti/workspace/ns-allinone-3.36.1/ns-3.36.1/cmake-cache/src/dsr/CMakeFiles/libdsr-obj.dir/model/dsr-network-queue.cc.o"
  "/home/drishti/workspace/ns-allinone-3.36.1/ns-3.36.1/src/dsr/model/dsr-option-header.cc" "/home/drishti/workspace/ns-allinone-3.36.1/ns-3.36.1/cmake-cache/src/dsr/CMakeFiles/libdsr-obj.dir/model/dsr-option-header.cc.o"
  "/home/drishti/workspace/ns-allinone-3.36.1/ns-3.36.1/src/dsr/model/dsr-options.cc" "/home/drishti/workspace/ns-allinone-3.36.1/ns-3.36.1/cmake-cache/src/dsr/CMakeFiles/libdsr-obj.dir/model/dsr-options.cc.o"
  "/home/drishti/workspace/ns-allinone-3.36.1/ns-3.36.1/src/dsr/model/dsr-passive-buff.cc" "/home/drishti/workspace/ns-allinone-3.36.1/ns-3.36.1/cmake-cache/src/dsr/CMakeFiles/libdsr-obj.dir/model/dsr-passive-buff.cc.o"
  "/home/drishti/workspace/ns-allinone-3.36.1/ns-3.36.1/src/dsr/model/dsr-rcache.cc" "/home/drishti/workspace/ns-allinone-3.36.1/ns-3.36.1/cmake-cache/src/dsr/CMakeFiles/libdsr-obj.dir/model/dsr-rcache.cc.o"
  "/home/drishti/workspace/ns-allinone-3.36.1/ns-3.36.1/src/dsr/model/dsr-routing.cc" "/home/drishti/workspace/ns-allinone-3.36.1/ns-3.36.1/cmake-cache/src/dsr/CMakeFiles/libdsr-obj.dir/model/dsr-routing.cc.o"
  "/home/drishti/workspace/ns-allinone-3.36.1/ns-3.36.1/src/dsr/model/dsr-rreq-table.cc" "/home/drishti/workspace/ns-allinone-3.36.1/ns-3.36.1/cmake-cache/src/dsr/CMakeFiles/libdsr-obj.dir/model/dsr-rreq-table.cc.o"
  "/home/drishti/workspace/ns-allinone-3.36.1/ns-3.36.1/src/dsr/model/dsr-rsendbuff.cc" "/home/drishti/workspace/ns-allinone-3.36.1/ns-3.36.1/cmake-cache/src/dsr/CMakeFiles/libdsr-obj.dir/model/dsr-rsendbuff.cc.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "CMAKE_EXAMPLE_AS_TEST"
  "HAVE_GSL"
  "HAVE_LIBXML2"
  "HAVE_SQLITE3"
  "NS3_ASSERT_ENABLE"
  "NS3_BUILD_PROFILE_DEBUG"
  "NS3_ENABLE_EXAMPLES"
  "NS3_LOG_ENABLE"
  "PROJECT_SOURCE_PATH=\"/home/drishti/workspace/ns-allinone-3.36.1/ns-3.36.1\""
  "__LINUX__"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "/usr"
  "../build/include"
  "/usr/include/freetype2"
  "/usr/include/glib-2.0"
  "/usr/lib/x86_64-linux-gnu/glib-2.0/include"
  "/usr/include/gdk-pixbuf-2.0"
  "/usr/include/gtk-3.0"
  "/usr/include/cairo"
  "/usr/include/pango-1.0"
  "/usr/include/harfbuzz"
  "/usr/include/atk-1.0"
  "/usr/include/libxml2"
  "/usr/include/python3.8"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
