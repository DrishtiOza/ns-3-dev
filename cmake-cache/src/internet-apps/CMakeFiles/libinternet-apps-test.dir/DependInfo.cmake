# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/drishti/workspace/ns-allinone-3.36.1/ns-3.36.1/src/internet-apps/test/dhcp-test.cc" "/home/drishti/workspace/ns-allinone-3.36.1/ns-3.36.1/cmake-cache/src/internet-apps/CMakeFiles/libinternet-apps-test.dir/test/dhcp-test.cc.o"
  "/home/drishti/workspace/ns-allinone-3.36.1/ns-3.36.1/src/internet-apps/test/ipv6-radvd-test.cc" "/home/drishti/workspace/ns-allinone-3.36.1/ns-3.36.1/cmake-cache/src/internet-apps/CMakeFiles/libinternet-apps-test.dir/test/ipv6-radvd-test.cc.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "CMAKE_EXAMPLE_AS_TEST"
  "HAVE_GSL"
  "HAVE_LIBXML2"
  "HAVE_SQLITE3"
  "NS3_ASSERT_ENABLE"
  "NS3_BUILD_PROFILE_DEBUG"
  "NS3_ENABLE_EXAMPLES"
  "NS3_LOG_ENABLE"
  "NS_TEST_SOURCEDIR=\"src/internet-apps/test\""
  "PROJECT_SOURCE_PATH=\"/home/drishti/workspace/ns-allinone-3.36.1/ns-3.36.1\""
  "__LINUX__"
  "libinternet_apps_test_EXPORTS"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "/usr"
  "../build/include"
  "/usr/include/freetype2"
  "/usr/include/glib-2.0"
  "/usr/lib/x86_64-linux-gnu/glib-2.0/include"
  "/usr/include/gdk-pixbuf-2.0"
  "/usr/include/gtk-3.0"
  "/usr/include/cairo"
  "/usr/include/pango-1.0"
  "/usr/include/harfbuzz"
  "/usr/include/atk-1.0"
  "/usr/include/libxml2"
  "/usr/include/python3.8"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/drishti/workspace/ns-allinone-3.36.1/ns-3.36.1/cmake-cache/src/internet-apps/CMakeFiles/libinternet-apps.dir/DependInfo.cmake"
  "/home/drishti/workspace/ns-allinone-3.36.1/ns-3.36.1/cmake-cache/src/internet/CMakeFiles/libinternet.dir/DependInfo.cmake"
  "/home/drishti/workspace/ns-allinone-3.36.1/ns-3.36.1/cmake-cache/src/bridge/CMakeFiles/libbridge.dir/DependInfo.cmake"
  "/home/drishti/workspace/ns-allinone-3.36.1/ns-3.36.1/cmake-cache/src/traffic-control/CMakeFiles/libtraffic-control.dir/DependInfo.cmake"
  "/home/drishti/workspace/ns-allinone-3.36.1/ns-3.36.1/cmake-cache/src/config-store/CMakeFiles/libconfig-store.dir/DependInfo.cmake"
  "/home/drishti/workspace/ns-allinone-3.36.1/ns-3.36.1/cmake-cache/src/network/CMakeFiles/libnetwork.dir/DependInfo.cmake"
  "/home/drishti/workspace/ns-allinone-3.36.1/ns-3.36.1/cmake-cache/src/stats/CMakeFiles/libstats.dir/DependInfo.cmake"
  "/home/drishti/workspace/ns-allinone-3.36.1/ns-3.36.1/cmake-cache/src/core/CMakeFiles/libcore.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
