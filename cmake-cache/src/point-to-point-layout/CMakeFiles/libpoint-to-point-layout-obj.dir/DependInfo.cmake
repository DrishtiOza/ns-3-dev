# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/drishti/workspace/ns-allinone-3.36.1/ns-3.36.1/src/point-to-point-layout/model/point-to-point-dumbbell.cc" "/home/drishti/workspace/ns-allinone-3.36.1/ns-3.36.1/cmake-cache/src/point-to-point-layout/CMakeFiles/libpoint-to-point-layout-obj.dir/model/point-to-point-dumbbell.cc.o"
  "/home/drishti/workspace/ns-allinone-3.36.1/ns-3.36.1/src/point-to-point-layout/model/point-to-point-grid.cc" "/home/drishti/workspace/ns-allinone-3.36.1/ns-3.36.1/cmake-cache/src/point-to-point-layout/CMakeFiles/libpoint-to-point-layout-obj.dir/model/point-to-point-grid.cc.o"
  "/home/drishti/workspace/ns-allinone-3.36.1/ns-3.36.1/src/point-to-point-layout/model/point-to-point-star.cc" "/home/drishti/workspace/ns-allinone-3.36.1/ns-3.36.1/cmake-cache/src/point-to-point-layout/CMakeFiles/libpoint-to-point-layout-obj.dir/model/point-to-point-star.cc.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "CMAKE_EXAMPLE_AS_TEST"
  "HAVE_GSL"
  "HAVE_LIBXML2"
  "HAVE_SQLITE3"
  "NS3_ASSERT_ENABLE"
  "NS3_BUILD_PROFILE_DEBUG"
  "NS3_ENABLE_EXAMPLES"
  "NS3_LOG_ENABLE"
  "PROJECT_SOURCE_PATH=\"/home/drishti/workspace/ns-allinone-3.36.1/ns-3.36.1\""
  "__LINUX__"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "/usr"
  "../build/include"
  "/usr/include/freetype2"
  "/usr/include/glib-2.0"
  "/usr/lib/x86_64-linux-gnu/glib-2.0/include"
  "/usr/include/gdk-pixbuf-2.0"
  "/usr/include/gtk-3.0"
  "/usr/include/cairo"
  "/usr/include/pango-1.0"
  "/usr/include/harfbuzz"
  "/usr/include/atk-1.0"
  "/usr/include/libxml2"
  "/usr/include/python3.8"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
