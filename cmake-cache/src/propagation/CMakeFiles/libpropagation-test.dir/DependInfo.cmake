# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/drishti/workspace/ns-allinone-3.36.1/ns-3.36.1/src/propagation/test/channel-condition-model-test-suite.cc" "/home/drishti/workspace/ns-allinone-3.36.1/ns-3.36.1/cmake-cache/src/propagation/CMakeFiles/libpropagation-test.dir/test/channel-condition-model-test-suite.cc.o"
  "/home/drishti/workspace/ns-allinone-3.36.1/ns-3.36.1/src/propagation/test/itu-r-1411-los-test-suite.cc" "/home/drishti/workspace/ns-allinone-3.36.1/ns-3.36.1/cmake-cache/src/propagation/CMakeFiles/libpropagation-test.dir/test/itu-r-1411-los-test-suite.cc.o"
  "/home/drishti/workspace/ns-allinone-3.36.1/ns-3.36.1/src/propagation/test/itu-r-1411-nlos-over-rooftop-test-suite.cc" "/home/drishti/workspace/ns-allinone-3.36.1/ns-3.36.1/cmake-cache/src/propagation/CMakeFiles/libpropagation-test.dir/test/itu-r-1411-nlos-over-rooftop-test-suite.cc.o"
  "/home/drishti/workspace/ns-allinone-3.36.1/ns-3.36.1/src/propagation/test/kun-2600-mhz-test-suite.cc" "/home/drishti/workspace/ns-allinone-3.36.1/ns-3.36.1/cmake-cache/src/propagation/CMakeFiles/libpropagation-test.dir/test/kun-2600-mhz-test-suite.cc.o"
  "/home/drishti/workspace/ns-allinone-3.36.1/ns-3.36.1/src/propagation/test/okumura-hata-test-suite.cc" "/home/drishti/workspace/ns-allinone-3.36.1/ns-3.36.1/cmake-cache/src/propagation/CMakeFiles/libpropagation-test.dir/test/okumura-hata-test-suite.cc.o"
  "/home/drishti/workspace/ns-allinone-3.36.1/ns-3.36.1/src/propagation/test/probabilistic-v2v-channel-condition-model-test.cc" "/home/drishti/workspace/ns-allinone-3.36.1/ns-3.36.1/cmake-cache/src/propagation/CMakeFiles/libpropagation-test.dir/test/probabilistic-v2v-channel-condition-model-test.cc.o"
  "/home/drishti/workspace/ns-allinone-3.36.1/ns-3.36.1/src/propagation/test/propagation-loss-model-test-suite.cc" "/home/drishti/workspace/ns-allinone-3.36.1/ns-3.36.1/cmake-cache/src/propagation/CMakeFiles/libpropagation-test.dir/test/propagation-loss-model-test-suite.cc.o"
  "/home/drishti/workspace/ns-allinone-3.36.1/ns-3.36.1/src/propagation/test/three-gpp-propagation-loss-model-test-suite.cc" "/home/drishti/workspace/ns-allinone-3.36.1/ns-3.36.1/cmake-cache/src/propagation/CMakeFiles/libpropagation-test.dir/test/three-gpp-propagation-loss-model-test-suite.cc.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "CMAKE_EXAMPLE_AS_TEST"
  "HAVE_GSL"
  "HAVE_LIBXML2"
  "HAVE_SQLITE3"
  "NS3_ASSERT_ENABLE"
  "NS3_BUILD_PROFILE_DEBUG"
  "NS3_ENABLE_EXAMPLES"
  "NS3_LOG_ENABLE"
  "NS_TEST_SOURCEDIR=\"src/propagation/test\""
  "PROJECT_SOURCE_PATH=\"/home/drishti/workspace/ns-allinone-3.36.1/ns-3.36.1\""
  "__LINUX__"
  "libpropagation_test_EXPORTS"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "/usr"
  "../build/include"
  "/usr/include/freetype2"
  "/usr/include/glib-2.0"
  "/usr/lib/x86_64-linux-gnu/glib-2.0/include"
  "/usr/include/gdk-pixbuf-2.0"
  "/usr/include/gtk-3.0"
  "/usr/include/cairo"
  "/usr/include/pango-1.0"
  "/usr/include/harfbuzz"
  "/usr/include/atk-1.0"
  "/usr/include/libxml2"
  "/usr/include/python3.8"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/drishti/workspace/ns-allinone-3.36.1/ns-3.36.1/cmake-cache/src/propagation/CMakeFiles/libpropagation.dir/DependInfo.cmake"
  "/home/drishti/workspace/ns-allinone-3.36.1/ns-3.36.1/cmake-cache/src/mobility/CMakeFiles/libmobility.dir/DependInfo.cmake"
  "/home/drishti/workspace/ns-allinone-3.36.1/ns-3.36.1/cmake-cache/src/network/CMakeFiles/libnetwork.dir/DependInfo.cmake"
  "/home/drishti/workspace/ns-allinone-3.36.1/ns-3.36.1/cmake-cache/src/stats/CMakeFiles/libstats.dir/DependInfo.cmake"
  "/home/drishti/workspace/ns-allinone-3.36.1/ns-3.36.1/cmake-cache/src/core/CMakeFiles/libcore.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
