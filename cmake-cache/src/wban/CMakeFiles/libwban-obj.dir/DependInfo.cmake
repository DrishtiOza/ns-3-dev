# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/drishti/workspace/ns-allinone-3.36.1/ns-3.36.1/src/wban/helper/wban-helper.cc" "/home/drishti/workspace/ns-allinone-3.36.1/ns-3.36.1/cmake-cache/src/wban/CMakeFiles/libwban-obj.dir/helper/wban-helper.cc.o"
  "/home/drishti/workspace/ns-allinone-3.36.1/ns-3.36.1/src/wban/model/wban-bch-encoder.cc" "/home/drishti/workspace/ns-allinone-3.36.1/ns-3.36.1/cmake-cache/src/wban/CMakeFiles/libwban-obj.dir/model/wban-bch-encoder.cc.o"
  "/home/drishti/workspace/ns-allinone-3.36.1/ns-3.36.1/src/wban/model/wban-error-model.cc" "/home/drishti/workspace/ns-allinone-3.36.1/ns-3.36.1/cmake-cache/src/wban/CMakeFiles/libwban-obj.dir/model/wban-error-model.cc.o"
  "/home/drishti/workspace/ns-allinone-3.36.1/ns-3.36.1/src/wban/model/wban-interference-helper.cc" "/home/drishti/workspace/ns-allinone-3.36.1/ns-3.36.1/cmake-cache/src/wban/CMakeFiles/libwban-obj.dir/model/wban-interference-helper.cc.o"
  "/home/drishti/workspace/ns-allinone-3.36.1/ns-3.36.1/src/wban/model/wban-lqi-tag.cc" "/home/drishti/workspace/ns-allinone-3.36.1/ns-3.36.1/cmake-cache/src/wban/CMakeFiles/libwban-obj.dir/model/wban-lqi-tag.cc.o"
  "/home/drishti/workspace/ns-allinone-3.36.1/ns-3.36.1/src/wban/model/wban-phy-header.cc" "/home/drishti/workspace/ns-allinone-3.36.1/ns-3.36.1/cmake-cache/src/wban/CMakeFiles/libwban-obj.dir/model/wban-phy-header.cc.o"
  "/home/drishti/workspace/ns-allinone-3.36.1/ns-3.36.1/src/wban/model/wban-phy.cc" "/home/drishti/workspace/ns-allinone-3.36.1/ns-3.36.1/cmake-cache/src/wban/CMakeFiles/libwban-obj.dir/model/wban-phy.cc.o"
  "/home/drishti/workspace/ns-allinone-3.36.1/ns-3.36.1/src/wban/model/wban-snr-model.cc" "/home/drishti/workspace/ns-allinone-3.36.1/ns-3.36.1/cmake-cache/src/wban/CMakeFiles/libwban-obj.dir/model/wban-snr-model.cc.o"
  "/home/drishti/workspace/ns-allinone-3.36.1/ns-3.36.1/src/wban/model/wban-spectrum-signal-parameters.cc" "/home/drishti/workspace/ns-allinone-3.36.1/ns-3.36.1/cmake-cache/src/wban/CMakeFiles/libwban-obj.dir/model/wban-spectrum-signal-parameters.cc.o"
  "/home/drishti/workspace/ns-allinone-3.36.1/ns-3.36.1/src/wban/model/wban-spectrum-value-helper.cc" "/home/drishti/workspace/ns-allinone-3.36.1/ns-3.36.1/cmake-cache/src/wban/CMakeFiles/libwban-obj.dir/model/wban-spectrum-value-helper.cc.o"
  "/home/drishti/workspace/ns-allinone-3.36.1/ns-3.36.1/src/wban/model/wban.cc" "/home/drishti/workspace/ns-allinone-3.36.1/ns-3.36.1/cmake-cache/src/wban/CMakeFiles/libwban-obj.dir/model/wban.cc.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "CMAKE_EXAMPLE_AS_TEST"
  "HAVE_GSL"
  "HAVE_LIBXML2"
  "HAVE_SQLITE3"
  "NS3_ASSERT_ENABLE"
  "NS3_BUILD_PROFILE_DEBUG"
  "NS3_ENABLE_EXAMPLES"
  "NS3_LOG_ENABLE"
  "PROJECT_SOURCE_PATH=\"/home/drishti/workspace/ns-allinone-3.36.1/ns-3.36.1\""
  "__LINUX__"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "/usr"
  "../build/include"
  "/usr/include/freetype2"
  "/usr/include/glib-2.0"
  "/usr/lib/x86_64-linux-gnu/glib-2.0/include"
  "/usr/include/gdk-pixbuf-2.0"
  "/usr/include/gtk-3.0"
  "/usr/include/cairo"
  "/usr/include/pango-1.0"
  "/usr/include/harfbuzz"
  "/usr/include/atk-1.0"
  "/usr/include/libxml2"
  "/usr/include/python3.8"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
